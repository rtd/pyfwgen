Introduction
------------

This set of flimsy classes automate the tasks of generating IP Table rules for a group of
crazy developers trying to hack your server.

Use JSON to config IP Table rules to insert into your dev server's iptable rules file.

Install
-------

pip install git+https://bitbucket.org/rtd/pyfwgen.git

Usage
-----

TBD


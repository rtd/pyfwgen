#!/usr/bin/python

import unittest
from IptableRuleGenerator import IptableRuleGenerator

class testIptableRuleGenerator(unittest.TestCase):

    def setUp(self):
        pass

    def testGenerator(self):
        iprules={ "Brad" : [ "0.0.0.0" ],
                         "Rob"  : [ "0.0.0.1" ] }
        ports=[ "8080", "21212" ]
        gen=IptableRuleGenerator(iprules=iprules,ports=ports)
        gen.generate()

if __name__ == '__main__':
    unittest.main()

